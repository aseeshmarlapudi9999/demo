import 'package:flutter/material.dart';

void main (){
  runApp(animated());
}

class animated extends StatefulWidget {
  @override
  _animatedState createState() => _animatedState();

}

class _animatedState extends State<animated> {
  double height = 200;
  double width = 200;
  Color color = Colors.blue;
  
  void changestate(){
    setState(() {
      height = 500;
      width = 300;
      color = Colors.red;
    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp (
      home: Scaffold(
        body: Center(
        child: AnimatedContainer(
          duration: Duration(milliseconds: 1000),
          curve: Curves.bounceOut ,
          height: height,
          width: width,

          decoration: BoxDecoration(
              color: color,
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          child: GestureDetector(
            onTap: changestate,
          ),
        ),
      ),
      ),
    );
  }
}
